 app.directive('hcChart', function() {
    return {
        restrict: 'E',
        template: '<div id="container" style="margin: 0 auto">not working</div',
        scope: {
            options: '='
        },
        link: function(scope, element) {
            var chart = new Highcharts.chart(element[0], scope.options);
            $(window).resize(function() {
                chart.reflow();
            });
        }
    };
})
//========================================================== DASHBOARD CONTROLLER============================================================================
//==============================================================================================================================================================
app.controller('dashboardController', function($http, $scope, $window,toastr, $log, $q, $timeout, $filter, mdcDateTimeDialog, $location) {
	   var auth_user_id = document.getElementById('auth_user_id').value;
	$('#loading').css('display', 'block');
  
  
    //GET  DATA FROM API=====================================================================
   
		var request = $http({
            method: "GET",
            url: APP_URL+'/v1/dashboard?auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.dashboardData = data;
			$scope.busy = data.busy; 
			$scope.store_id = data.store_id; 
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
			
			 $scope.orders_x = $scope.dashboardData.blocks[5];
     
	 $scope.orders_x_data = $scope.orders_x[0].data;  
	   var d_array_labels = [];
      var d_array_values = []; 
       angular.forEach($scope.orders_x_data, function(value, key) {
        d_array_labels.push(value.label);
        d_array_values.push(value.y);
    }, '');
	
	document.getElementById("res1").value = d_array_labels;
	document.getElementById("res2").value = d_array_values; 
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	 
  
 
     $scope.ordersChartOptions = { 
		chart: { 
		        height: 300 + 'px', // 16:9 ratio
		},
        title: {
            text: ''
        },
        xAxis: {
            gridLineWidth: 0,
            tickMarks: 0,
            tickInterval: 1,
             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: [{ // Primary yAxis 
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
				minPadding: 0,
                maxPadding: 0
            }
        }],
        credits: {
            enabled: false
        },
        legend: {
            enabled: false,
        },
        exporting: {
            enabled: false
        },

        // legend: { },
        options: {
            chart: {  
                type: 'line'
            }
        },
        colors: ['#a2cd3b', '#babbbd'],
        tooltip: {
            backgroundColor: '#f5f5f5',
            borderColor: '#fff',
            borderRadius: 10,
            borderWidth: 1,
            crosshairs: true,
           /* formatter: function() {
                var point = this.points[0];
                return '<b>' + point.series.name + '</b><br/>' + Highcharts.dateFormat('%A %B %e %Y', this.x) + ': <br/>' +
                    '1 USD = ' + Highcharts.numberFormat(point.y, 2) + ' EUR';
            },*/
            shared: true
        },
        series: [{
			name:'Orders',
            type: 'areaspline',
            data:  [0,0,0,0,0,0,0,0,0,9]
        }, {
			name:'Orders',
            type: 'line',
            data:   [0,0,0,0,0,0,0,0,0,9]
        }],

    };
	
	
	
	$scope.switchStoreStatus = function(value){
		 
		 if(value == 0){ 
			$scope.store_id_value =  { "store_id":$scope.store_id }
			 
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/make-store-unbusy/'+$scope.store_id,
            data:  $scope.store_id_value,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data = data;   
			console.log(JSON.stringify(data));
			 if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }
		})
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		 }
		 
		 else{
			 $('#myModal').modal('show'); 
		 } 
			 
	}
	
	$scope.storeStatusBusy = function(){
		
		$scope.start_time = $('#start_time').val();
		 
		$scope.end_time = $('#end_time').val();
		 
		$scope.reason = $('#reason').val();
		$scope.store_data =  { "start_time": $scope.start_time, "end_time":$scope.end_time, "store_id":$scope.store_id,  "reason":$scope.reason };
			 
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/make-store-busy/'+$scope.store_id,
            data:  $scope.store_data,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.data = data;   
			$('#storeStatus').modal('hide'); 
			console.log(JSON.stringify(data));
			 if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); location.reload();}
            else { toastr.error(data.message, 'Error'); return false; }
		})
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		
	}

	
});