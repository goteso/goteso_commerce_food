
app.config(function($routeProvider) {
    $routeProvider 
    .when("/", {
        templateUrl : APP_URL+"/order-regular.html",
        controller : "regularOrdersController as ctrl"
    })
    .when("/catering", {
        templateUrl : APP_URL+"/order-catering.html",
        controller : "cateringOrdersController as ctrl"
    })
    .otherwise({
            redirectTo: '/'
        });	;
});
		
		
		 
//==========================================================REGULAR ORDER CONTROLLER==================================================================================
//==============================================================================================================================================================
 
	
	 app.controller('regularOrdersController', function($http,$scope,$window,$log, toastr) {
		  
	 var pro = this; 
	    $('#loading').css('display', 'block');
	    var auth_user_id = document.getElementById('auth_user_id').value;
	   
		//========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
        var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_user_id='+auth_user_id+'&status=any&order_type=regular',  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			pro.orders =  data ; 
            document.getElementById('res').value = JSON.stringify(data); 


            $('#loading').css('display', 'none');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
 
 
 
        pro.getOrdersByStatus = function(value){
			 
			pro.order_status = value;
			
			  var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_user_id='+auth_user_id+'&status='+pro.order_status,  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			pro.orders =  data ; 
            document.getElementById('res').value = JSON.stringify(data); 


            $('#loading').css('display', 'none');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
		}
  
  
  // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&include_count_blocks=true&auth_user_id='+auth_user_id+'&status='+pro.order_status,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.orders = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
  
		
	});







//==========================================================CATERING ORDER CONTROLLER==================================================================================
//==============================================================================================================================================================
 
	
	 app.controller('cateringOrdersController', function($http,$scope,$window,$log, toastr) {
		  
	 var pro = this; 
	    $('#loading').css('display', 'block');
	    var auth_user_id = document.getElementById('auth_user_id').value;
	   
		//========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
        var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_user_id='+auth_user_id+'&status=any&order_type=catering',  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			pro.orders =  data ; 
            document.getElementById('res').value = JSON.stringify(data); 


            $('#loading').css('display', 'none');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
 
 
 
        pro.getOrdersByStatus = function(value){
			 
			pro.order_status = value;
			
			  var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_user_id='+auth_user_id+'&status='+pro.order_status,  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			pro.orders =  data ; 
            document.getElementById('res').value = JSON.stringify(data); 


            $('#loading').css('display', 'none');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
		}
  
  
  // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&include_count_blocks=true&auth_user_id='+auth_user_id+'&status='+pro.order_status,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.orders = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
  
		
	});


 


