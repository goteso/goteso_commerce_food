 
	
 //====================================================== REVIEW CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('reviewsController', function ($http, $scope, $window,toastr) {
 
	     // get basic form to add product ============================================================== clean done
	     
 
         $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/order-review',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.reviews  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  



		
		 //================================ FUNCTION FOR DELETE TIMELOT  ================================================================================
	 
    	 $scope.deleteReview = function(id) { 
			$scope.review_id = id; 
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/order-review/'+$scope.review_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.reviews = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
		
	});
		
		  
		
		
		 