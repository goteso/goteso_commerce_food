<?php

namespace App\Http\Controllers\Admin; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\Admin;
use Mail;
use Hash;
 
 
use App\Traits\one_signal; // <-- you'll need this line...


class BrandsController extends Controller 
{
    
  use one_signal; 
    
   
   public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(request $request)
    {
	 
	  // get all the users
        $users = Admin::where("user_type",'brand')->orderBy('id','desc')->paginate(10);  
		if ($request->ajax()) {
            return view('admin.brand.data-ajax', compact('users'));
        }
 		return view('admin.brand.index')->with('users', $users);
    }
	
	
	    public function stores_list( Request $request , $id)
    {
	 
		 
	  // get all the users
        $users = Admin::where("user_type",'store')->where("store_brand_id",$id)->orderBy('id','desc')->paginate(10);  
		
	 
		$brand_title = \App\Admin::where("id",$id)->first(["first_name"])->first_name;
		if ($request->ajax()) {
            return view('admin.stores.data-ajax', compact('users','brand_title','id'));
        }
		
 
 		return view('admin.stores.index')->with('users', $users)->with("brand_title",$brand_title)->with("id",$id);
    }
	
		   
		   public function user_by_status(request $request, $status)
    {
		// get all the users
        $users = Admin::where("user_type",'brand')->where('status',$status)->orderBy('id','desc')->paginate(10);  
		if ($request->ajax()) {
            return view('admin.brand.data-ajax', compact('users'));
        }
 		return view('admin.brand.index')->with('users', $users );
    }
	 
	public function user_by_verified(request $request, $verified)
    {
		   // get all the users
        $users = Admin::where("user_type",'brand')->where('verified',$verified)->orderBy('id','desc')->paginate(10);  
		if ($request->ajax()) {
            return view('admin.brand.data-ajax', compact('users'));
        }
 		return view('admin.brand.index')->with('users', $users );
    }
	
	

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
		
		
      $countries = DB::table('countries')->get();        
      return view('admin.brand.create')->with('countries',$countries);
    }
	
     public function create_store($id)
    {
      $countries = DB::table('countries')->get();        
      return view('admin.stores.create')->with('countries',$countries)->with('id',$id);
    }
	
	    public function add_store(Request $request,$id)
    {
	 
		 
        $validator = Validator::make($request->all(), [
        'email' => 'required|email|max:255|unique:users',
        'password'  =>  'required|min:6|confirmed|alpha_num',
        
         ]);
        if ($validator->fails()) {
          //print_r($validator->errors()->all());die;
            return redirect('/brands/brand/'.$id.'/stores/create')
                ->withInput()
                ->withErrors($validator);
        }
        $user = new Admin;
   
 
        $user->email  = $request->email;
        $user->user_type = 'store';
        $user->password  =  Hash::make($request->password);
        $user->status = $request->status;
		$user->short_address = $request->short_address;
		$user->long_address = $request->long_address;
		$user->store_brand_id = $id;
		$user->store_notification_text = $request->store_notification_text;
		$user->store_beacon_UDID = $request->store_beacon_UDID;
		$user->latitude = $request->latitude;
		$user->longitude = $request->longitude;
		$random_string =str_random(100);
		\QrCode::format('png')->size(800)->generate( $random_string  , '../public/qrcodes/'.time().'.png');
	    $user->store_qrcode = 'qrcodes/'.time().'.png';
		$user->store_qrcode_string = $random_string;
		
        $user->save();
 
 
        // redirect
        Session::flash('message', 'Successfully created store!');
        return redirect('/brands/brand/'.$id.'/stores/');
    }
	
	

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		 
		 
        $validator = Validator::make($request->all(), [
      
        
        'email' => 'required|email|max:255|unique:users',
        'password'  =>  'required|min:6|confirmed|alpha_num',
        
         ]);
        if ($validator->fails()) {
          //print_r($validator->errors()->all());die;
            return redirect('/brands/brand/create')
                ->withInput()
                ->withErrors($validator);
        }
		
		$t=time();
			   
	   if($request->hasFile('image'))
	   {
		   
		 
		     $file = $request->file('image');
       //Move Uploaded File
       $destinationPath = 'admin/brands/';
       $file->move($destinationPath,$file->getClientOriginalName());
	   $file_name = $destinationPath.$file->getClientOriginalName(); 
	    
	    $this->make_thumb(public_path().'/'.$file_name,public_path('/').'/admin/brands/brand'.$t.'.jpg','200');
	   $photo ='admin/brands/brand'.$t.'.jpg';
	   }
	   else
		   
		   {
			 
			   $photo ='';
		   }
	   

        $user = new Admin;
        $user->first_name  = $request->first_name;
 
        $user->email  = $request->email;
		$user->photo  = $photo;
        $user->user_type = 'brand';
        $user->password  =  Hash::make($request->password);
  
        $user->status = 1;
        $user->save();
 
 
        // redirect
        Session::flash('message', 'Successfully created brand!');
        return redirect('brands');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {   
        if(!is_numeric($id))
        {
         return redirect('brands');   
        }
        // get the testimonial
        $user = Admin::where('id',$id)->first(['id','first_name','email', 'status']);
       // show the view and pass the nerd to it
        return view('admin.brand.show')
            ->with('user', $user);
    }
	
	
	    public function show_store($id)
    {   
        if(!is_numeric($id))
        {
         return back();  
        }
        // get the testimonial
        $user = Admin::where('id',$id)->first(['email', 'store_notification_text','store_beacon_UDID','latitude','longitude','short_address','long_address']);
       // show the view and pass the nerd to it
        return view('admin.stores.show')
            ->with('user', $user);
    }
	
	

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if(!is_numeric($id))
        {
         return redirect('brands');   
        }
        $countries = DB::table('countries')->get();   
        $user = Admin::find($id);

        // show the edit form and pass the nerd
        return view('admin.brand.edit')
            ->with('user', $user)->with('countries', $countries);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id , Request $request)
    {
		
                // validate
        if(!is_numeric($id))
        {
         return redirect('brands');   
        }
  $t = time();
  
  	   if($request->hasFile('image'))
	   {
		   
		  
		 
		     $file = $request->file('image');
       //Move Uploaded File
       $destinationPath = 'admin/brands/';
       $file->move($destinationPath,$file->getClientOriginalName());
	   $file_name = $destinationPath.$file->getClientOriginalName(); 
	    
	    $this->make_thumb(public_path().'/'.$file_name,public_path('/').'/admin/brands/brand'.$t.'.jpg','200');
	   $photo ='admin/brands/brand'.$t.'.jpg';
	   App\Admin::where('id', $id)->update(['photo' => $photo]);
	   }
	   else
		   
		   {
			 
			   $photo ='';
		   }
		   
		   
  
            // store
            $user = Admin::find($id);
            $user->first_name  = $request->first_name;
		 
            $user->email   = $request->email;
            $user->status  = $request->status;
                                        
            $user->save();

            // redirect
            Session::flash('message', 'Successfully updated brand!');
            return redirect('brands');
      
    }
	
	
	
	
	
	    public function edit_store($id)
    {
        if(!is_numeric($id))
        {
         return redirect('brands');   
        }
        $countries = DB::table('countries')->get();   
        $user = Admin::find($id);
		
		

        // show the edit form and pass the nerd
        return view('admin.stores.edit')
            ->with('user', $user)->with('countries', $countries)->with('id', $id);
			
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update_store($id , Request $request)
    {
		
		 
                // validate
        if(!is_numeric($id))
        {
         return redirect('brands');   
        }
  
  
            // store
            $user = Admin::find($id);
      
			
			
			    $user->email  = $request->email;
        $user->user_type = 'store';
    
        $user->status = $request->status;
		$user->short_address = $request->short_address;
		$user->long_address = $request->long_address;
		 
		$user->store_notification_text = $request->store_notification_text;
		$user->store_beacon_UDID = $request->store_beacon_UDID;
		$user->latitude = $request->latitude;
		$user->longitude = $request->longitude;
	 
		
        $user->save();

            // redirect
            Session::flash('message', 'Successfully updated Store!');
            return back();
      
    }
	
	
	
	
	
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		$user = Admin::find($id);
        $user->delete();
        Session::flash('message', 'Successfully deleted the recod!');
        return back();
    }
	public function block($id)
	{
	        $user = Admin::find($id);
            $user->status  = '0';
            $user->save();
			Session::flash('message', 'Successfully Blocked the brand!');
			
			//$notification_token= Admin::where('id',$id)->first(["notification_token"])->notification_token;
	        //$this->notification_to_single($notification_token, 'You are Blocked' , 'You are blocked from loyatyApp by Admin');
				
		    return back();
	}
		public function unblock($id)
	{
	        $user = Admin::find($id);
            $user->status  = '1';
            $user->save();
			Session::flash('message', 'Successfully Unblocked the brand!');
			
			//$notification_token= Admin::where('id',$id)->first(["notification_token"])->notification_token;
	       // $this->notification_to_single($notification_token, 'You are Unblocked' , 'You are Unblocked from loyatyApp by Admin');
		 		 
			return back();
	}
	
	
		public function verify($id)
	{
	        $user = Admin::find($id);
            $user->verified  = '1';
            $user->save();
			Session::flash('message', 'Admin Verified Successfully');
			
			//$notification_token= Admin::where('id',$id)->first(["notification_token"])->notification_token;
	       // $this->notification_to_single($notification_token, 'You are Verified' , 'You are Successfully Verified by loyatyApp Admin');
				
		    return back();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }
	
	
	
	
	

}