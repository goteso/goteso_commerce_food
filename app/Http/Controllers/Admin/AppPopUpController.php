<?php

namespace App\Http\Controllers\Admin; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Mail;
use Hash;
use App\AppPopUp;
 


class AppPopUpController extends Controller 
{
    
   
   public function __construct()
    {
        $this->middleware('auth');
    }

 
    public function index(request $request)
    {
		
		 
        // get all the users
        $pop_up_content = AppPopUp::where('key_name','app_pop_up')->first(["pop_up_content"])->pop_up_content;
		
 		return view('admin.app_pop_up.index')->with('content', $pop_up_content);
    }
 
 
 
 
     public function pop_up_content_update(request $request)
    {	  
	  $popUp = AppPopUp::firstOrNew(array('key_name' => 'app_pop_up'));
      $popUp->pop_up_content = $request->pop_up_content;	 
	  $popUp->save();	 
      return view('admin.app_pop_up.index')->with('content', $popUp->pop_up_content);
    }
 
 
 
	

}