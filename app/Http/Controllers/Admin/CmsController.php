<?php

namespace App\Http\Controllers\Admin; //admin add

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\Cms;


class CmsController extends Controller 
{
	
   
   public function __construct()
    {
        $this->middleware('auth');
    }

  /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the cms        
        $cms = DB::table('cms')->paginate(5);       
        // load the view and pass the cms
        return view('admin.cms.index')->with('cms', $cms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      return view('admin.cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'title' => 'required|max:255',
        'content' => 'required',
         ]);
        if ($validator->fails()) {
          //print_r($validator->errors()->all());die;
            return redirect('/admin/cms/create')
                ->withInput()
                ->withErrors($validator);
        }
        $cms = new Cms;
        $cms->title  = $request->title;
        $cms->content   = $request->content;
        $cms->status = 1;
        $cms->save();
        // redirect
        Session::flash('message', 'Successfully created Cms!');
        return redirect('admin/cms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the testimonial
        $cms = Cms::find($id);

        // show the view and pass the nerd to it
        return view('admin.cms.show')
            ->with('cms', $cms);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $cms = Cms::find($id);

        // show the edit form and pass the nerd
        return view('admin.cms.edit')
            ->with('cms', $cms);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id , Request $request)
    {
                // validate
        $validator = Validator::make($request->all(), [
        'title' => 'required|max:255',
        'content' => 'required',
         ]);
        if ($validator->fails()) {
          //print_r($validator->errors()->all());die;
            return redirect('/admin/cms/'.$id.'/edit')
                ->withInput()
                ->withErrors($validator);
        }
         else {
            // store
            $cms = Cms::find($id);
            $cms->title  = $request->title;
            $cms->content   = $request->content;
            $cms->status   = $request->status;
            $cms->save();

            // redirect
            Session::flash('message', 'Successfully updated cms!');
            return redirect('admin/cms');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $cms = Cms::find($id);
        $cms->delete();
        // redirect
        Session::flash('message', 'Successfully deleted the cms!');
        return redirect('admin/cms');
    }

}