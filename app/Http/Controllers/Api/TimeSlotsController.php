<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class TimeSlotsController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
   
   
 // Route-9.1 ============================================================== Store TimeSlot to timeslots table =========================================> 
   public function store(Request $request)
   {
                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'weekday' => 'required',
					'from_time' => 'required',
					'to_time' => 'required',
			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			 
					$category = new \App\TimeSlots;
					$category->weekday = $this->validate_string(@$request->weekday);
					$category->from_time = $this->validate_string($request->from_time);
					$category->to_time = $this->validate_integer($request->to_time);
                    $category->save();
					
				    if($category != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'TimeSlot Added Successfully';
                          $data['data']      =   $category;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add TimeSlot';
                          $data['data']      =   [];  
					}
				   
				  return $data;
				 
  }
   

   
  
  // Route-9.2 ============================================================== Get Categories List =========================================> 
   public function get_list()
   {
	   
        $per_page = $this->get_variable_per_page(); //ASC or DESC
		    $from_date = $this->get_variable_from_date();
		    $include_empty_date = $this->get_variable_include_empty_date();
        $todays_date_formatted = \Carbon\Carbon::now()->format('d-m-Y');
		    $from_date_formatted = \Carbon\Carbon::parse($from_date)->format('d-m-Y');

		   
	   
 
                     $timeslots_array = array();


                     $day_name = \Carbon\Carbon::parse($from_date)->format('D');   


                        $d['day_name'] = $day_name;
                     
                     $d['date'] =   @\Carbon\Carbon::parse($from_date)->format('d M,Y')."";
                     

 
                     $timeslots = @\App\TimeSlots::where('weekday',$day_name)->get(); 
                     $today_filtered_timeslots = array();
                     foreach($timeslots as $ts)
                     {

                     	        $current_time = @\Carbon\Carbon::now()->format('H:i:s');
                              $s = $ts->from_time;
                              $db_time_from = strtotime($s);
                              $db_time_from = date('H:i:s', $db_time_from);
                              $time_diff = strtotime($current_time)- strtotime($db_time_from);


 

                            
                              if($from_date_formatted ==  $todays_date_formatted )
                              {
                                  if($time_diff < 0)
                                  {
                                      $today_filtered_timeslots[] = $ts;
                                  }
                              }
                              else
                              {
                              	 		   if(strtotime($from_date_formatted) >=  strtotime($todays_date_formatted) )
                                		    {
                                		     	 $today_filtered_timeslots[] = $ts;
                              				}
                              }
                          

                     }

                     $d['slots'] = $today_filtered_timeslots;
                      


 

                      if(strtotime($todays_date_formatted) <= strtotime($from_date_formatted) )
                      {
                             if( $include_empty_date == 'true'  )
                        	  {
                          		  $timeslots_array[] = $d;
                       		  }
                             else
                              {
                           		 if(sizeof($today_filtered_timeslots) > 0)
                     	  		 {
                     	   	  		$timeslots_array[] = $d;
                     	  		 }
                      		  }
                      }

  
        $days_difference = 10;
     
        $from_date2 = \Carbon\Carbon::parse($this->get_variable_from_date());
 
        for($i=1;$i<=$days_difference;$i++)
        { 
                    $from_date2 = $from_date;
 
        	         $new_date = \Carbon\Carbon::parse($from_date2)->addDays($i);
        	         $day_name = $new_date->format('D');
        	         $new_date_formatted = \Carbon\Carbon::parse($new_date)->format('d-m-Y');   

      
        	         if(strtotime($todays_date_formatted) <= strtotime($new_date_formatted) )
                             {
                     
                                $d['day_name'] = $day_name;
                                $d['date'] = @\Carbon\Carbon::parse($new_date)->format('d M,Y')."";
                                $d['date_formatted'] = @\Carbon\Carbon::parse($new_date)->format('d M,Y')."";

                                $timeslots = @\App\Timeslots::where('weekday',$day_name)->get(); 
                                $today_filtered_timeslots = array();
                     

                     foreach($timeslots as $ts)
                     {
                     	      $current_time = @\Carbon\Carbon::now()->format('H:i:s');
                              $s = $ts->from_time;
                              $db_time_from = strtotime($s);
                              $db_time_from = date('H:i:s', $db_time_from);
                              $time_diff = strtotime($current_time)- strtotime($db_time_from);

                              $todays_date_formatted = \Carbon\Carbon::now()->format('d-m-Y');
		                          $from_date_formatted = \Carbon\Carbon::parse($from_date2)->format('d-m-Y');


        
                     

                              if($new_date_formatted ==  $todays_date_formatted )
                              {
                  
                                if($time_diff < 1)
                                 {

                              	   $today_filtered_timeslots[] = $ts;
                                 }
                              }
                              else
                              {
 
                              	$today_filtered_timeslots[] = $ts;
                              }
                           }

 
                        $d['slots'] = $today_filtered_timeslots;
                     	   
                     	  
                    

                        $timeslots = @\App\Timeslots::where('weekday',$day_name)->get(); 


 
                        if( $include_empty_date == 'true'  )
                        {

                          $timeslots_array[] = $d;
                       }
                       else
                       {
                            if(sizeof($today_filtered_timeslots) > 0)
                     	   {
                     	   	  $timeslots_array[] = $d;
                     	   }
                       }
                     
}
                     
                     
        }
	   
	              if(sizeof($timeslots_array) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'TimeSlots Fetched Successfully';
                          $data['data']      =   $timeslots_array;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No TimeSlots Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  



  // Route-9.3 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id)
   {
	   
					$validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'weekday' => 'required',
					'from_time' => 'required',
					'to_time' => 'required',
					]);
	   
					if($validator->errors()->all()) 
					{
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   $validator->errors()->first();
						return $data;					
					}				
				
	               //check existance of category with ID in categories table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'TimeSlot with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
				    
					$weekday = $this->validate_string(@$request->weekday);
					$from_time = $this->validate_string($request->from_time);
					$to_time = $this->validate_integer($request->to_time);
				 
	                App\TimeSlots::where('id', $id)->update(['weekday' => $weekday ,'from_time' => $from_time , 'to_time' => $to_time  ]);
	               
				    $result = @\App\TimeSlots::where('id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'TimeSlot Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
					}
				   return $data;
   }  


   
 

 // Route-9.4 ============================= delete a TimeSlots================================
   public function destroy(Request $request, $id)
   {
 
     
             //check existance of item with ID in items table
          $exist = $this->model_exist($id); 
                    if($exist == 0 or $exist == '0')
                    {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'TimeSlot with this ID does not exist';
                          $data['data']      =   [];
                          return $data;             
                    }
 
                      @\App\TimeSlots::where('id',$id)->delete();
 
 

                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'TimeSlot Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
    
   }
 
 
 
 
 //Route-9.5 ================================== Get List for Admin Panel Settings =========================================

   public function get_list_settings()
   {
    $main_array = array();

    $d['day'] = 'Mon';
    $d['slots'] = @\App\TimeSlots::where('weekday','Mon')->get();

    $form_fields_array = array();



    $main_array[] = $d;

    $d['day'] = 'Tue';
    $d['slots'] = @\App\TimeSlots::where('weekday','Tue')->get();
    $main_array[] = $d;

    $d['day'] = 'Wed';
    $d['slots'] = @\App\TimeSlots::where('weekday','Wed')->get();
    $main_array[] = $d;

    $d['day'] = 'Thu';
    $d['slots'] = @\App\TimeSlots::where('weekday','Thu')->get();
    $main_array[] = $d;

    $d['day'] = 'Fri';
    $d['slots'] = @\App\TimeSlots::where('weekday','Fri')->get();
    $main_array[] = $d;

    $d['day'] = 'Sat';
    $d['slots'] = @\App\TimeSlots::where('weekday','Sat')->get();
    $main_array[] = $d;

    $d['day'] = 'Sun';
    $d['slots'] = @\App\TimeSlots::where('weekday','Sun')->get();
    $main_array[] = $d;

 
    //form starts

    $fields_array = array();
/**
   // $d2['title'] = 'Weekday';
    $d2['identifier'] = 'weekday';
    $d2['type'] = 'hidden';
    $d2['value'] = '';
    $fields_array[] = $d2;
**/
    $d2['title'] = 'Start Time';
    $d2['identifier'] = 'from_time';
    $d2['type'] = '';
    $d2['value'] = '';
    $fields_array[] = $d2;

    $d2['title'] = 'End Time';
    $d2['identifier'] = 'to_time';
    $d2['type'] = '';
    $d2['value'] = '';
    $fields_array[] = $d2;
 
 


    //from ends



                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'TimeSlot Settings Fetched Successfully';
                          $data['data']      =   $main_array;  
                          $data['form_data']      =   $fields_array;  
                          return $data;
   }
 
 
 









//// Route- get timeslots form


public function get_edit_form()
{


/**
$timeslots_distinct_weekday = @\App\TimeSlots::select('weekday')->distinct()->pluck('weekday');


 

if(sizeof($timeslots_distinct_weekday) > 0 )
{

   for($t=0; $t < sizeof($timeslots_distinct_weekday) ;  $t++)
   {

      $timeslots = @\App\TimeSlots::where('weekday' , $timeslots_distinct_weekday[$t] )->get();

      foreach($timeslots as $timeslot)
      {
              $fields_array = array();
              $d2['title'] = 'Start Time';
              $d2['identifier'] = 'price';
              $fields_array_variants[] = $d2; 

      }

   }

}


**/

/**
      $distinct_variants_types = \App\ItemVariantType::get();
    $variants_array = array();
    foreach($distinct_variants_types as $vt)
    { 
         $cols_array = array();
    
       $tabs = array();
       $fields_array_variants = array();
     $data_array3["title"] =   $vt->item_variant_type_title;
     
     $d1['title'] = 'Title';
     $d1['type'] = 'text';
     $d1['identifier'] = 'title';
       $fields_array_variants[] = $d1;
     
     $col_array[] = 'item_variant_value_id';
     $col_array[] = 'item_variant_value_title';
     
     
       $col_array[] = 'item_variant_price';
       $d2['title'] = 'Price';
       $d2['type'] = 'float';
       $d2['identifier'] = 'price';
       $d2['symbol'] = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;
       $fields_array_variants[] = $d2; 
      
      
   
     
     
       $col_array[] = 'item_variant_price_difference';
       $d4['title'] = 'Price Difference';
       $d4['type'] = 'float';
       $d4['identifier'] = 'price_difference';
         $d4['symbol'] = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;
       $fields_array_variants[] = $d4;
       
      $vd['title'] =   $vt->item_variant_type_title;
      $vd['product_variant_type_id'] =   $vt->item_variant_type_id;
      $vd['fields'] =   $fields_array_variants;
      $vd['values'] =   @\App\ItemVariantValue::where('item_variant_type_id',$vt->item_variant_type_id)->where('item_id',$id)->get($col_array);
      $variants_array[] = $vd;

    }
    
       $data_array3['title'] = 'Variants';
     $data_array3['type'] = 'item_variants';
       $data_array3['fields'] = $variants_array;
    $main_array[] =$data_array3;
    **/


}













 
 
 
 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function model_exist($id)
{
	$count = @\App\TimeSlots::where('id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_from_date()
{
	 if(isset($_GET['from_date']) && $_GET['from_date'] != null && $_GET['from_date'] != '')
					{ $from_date = @\Carbon\Carbon::parse($_GET['from_date'])->format('Y-m-d H:i:s'); }
					else 
					{ $from_date = @\Carbon\Carbon::now(); }
    return $from_date;
}

public function get_variable_include_empty_date()
{
	 if(isset($_GET['include_empty_date']) && $_GET['include_empty_date'] != null && $_GET['include_empty_date'] != '')
					{ $include_empty_date = $_GET['include_empty_date']; }
					else 
					{ $include_empty_date = 'false'; }
    return $include_empty_date;
}




 
  
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}