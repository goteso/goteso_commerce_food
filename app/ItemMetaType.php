<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMetaType extends Model
{
	
protected $fillable = ['item_meta_type_title','identifier','limit','type','meta_type'];
protected $table = 'item_meta_type';
 
}
