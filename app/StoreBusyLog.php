<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreBusyLog extends Model
{
        protected $fillable = [
        'id','store_id','start_time','end_time','reason'];

        protected $table = 'store_busy_log';



         public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	

     public function getStoreDetailsAttribute($value) {
         return  @\App\Stores::where('store_id' , $this->store_id)->get(['store_id','store_title','store_photo','store_rating','address']);
    }

	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
     
 
}
