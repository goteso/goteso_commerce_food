<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTransactionExtra extends Model
{
        protected $fillable = [ 'order_id' , 'setting_transaction_extra_id', 'setting_transaction_extra_title' , 'order_transaction_extra_amount' ];
		protected $table = 'order_transaction_extra';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}