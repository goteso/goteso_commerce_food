<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavouriteItem extends Model
{
        protected $fillable = [ 'id' , 'user_id', 'item_id' ];
        protected $table = 'favourite_item';
        
 
    
        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }


 public function getItemDetailsAttribute($value) {
         $id = $this->item_id;
         $details = @\App\Items::where('item_id',$id)->get();
         if(sizeof($details) > 0)
         {
           return $details;
         }
         else
         {
            return [];
         }
    }


 public function getUserDetailsAttribute($value) {
         $user_id = $this->user_id;
         $user_details = @\App\User::where('user_id',$user_id)->get();
         if(sizeof($user_details) > 0)
         {
           return $user_details;
         }
         else
         {
            return [];
         }
    }

 
    
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
    
    
}