<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationSettings extends Model
{
  protected $fillable = ['id', 'user_id','offer_notification'];
  protected $table = 'notification_settings';
}
