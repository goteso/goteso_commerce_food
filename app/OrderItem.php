<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
        protected $fillable = [ 'order_id' , 'item_id', 'item_title' , 'order_item_quantity' , 'order_item_unit' , 'item_price' , 'order_item_discount' ];
		protected $table = 'order_item';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }





         public function getSingleQuantityItemPriceAttribute($value) {
         $single_quantity_item_price =  floatval($this->item_price) / floatval($this->order_item_quantity);
         return round($single_quantity_item_price , 2 );
    }


	

 

	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}