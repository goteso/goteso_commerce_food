<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
        protected $fillable = [ 'name' , 'guard_name', 'admin_panel_login' , 'sidebar_status' ];
		protected $table = 'roles';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}