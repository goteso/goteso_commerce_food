<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoyaltyPoints extends Model
{
	 protected $fillable = [
        'user_id','type','points','expiry_date','source' ];
       
	  public function user()
    {
    return $this->belongsTo('App\User', 'notification_by_user_id');
    }

protected $casts = [ "points"=>"int" ];
	
	
 
	

			    public function getCreatedAtAttribute($value) {
         $v = \Carbon\Carbon::parse($value)->diffforhumans();
		 
		 
	 
        $v = str_replace([' seconds', ' second'], 'sec', $v);
        $v = str_replace([' minutes', ' minute'], 'min', $v);
        $v = str_replace([' hours', ' hour'], 'h', $v);
        $v = str_replace([' months', ' month'], 'm', $v);
		$v = str_replace([' days', ' month' , ' day'], 'd', $v);
		$v = str_replace([' weeks', ' week'], 'w', $v);
		$v =  str_replace([' ago', ' ago'], '', $v);

        if(preg_match('(years|year)', $v)){
            $v = $v->toFormattedDateString();
        }

        return $v;
		
		
    }


	
}
