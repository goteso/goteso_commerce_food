<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationTriggers extends Model
{
  protected $fillable = ['trigger_type','notification_type','title','sub_title' ];
  protected $table = 'notification_triggers';
}
